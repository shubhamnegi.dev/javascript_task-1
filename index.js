const form = document.getElementById("form");
const container = document.getElementsByClassName("container");

/**
 * getting selected dropdown value;
 */
const dropdown = document.getElementById("dropdown");

var markedCheckbox = document.querySelectorAll('input[type="checkbox"]');

const handleSubmit = (e) => {
  e.preventDefault();
  let count = 0;

  markedCheckbox.forEach((item) => {
    if (item.checked) {
      count++;
    }
  });

  if (count < 2) {
    window.alert("Select atleast two perm");
  } else {
    form.style.display = "none";

    const email = document.getElementById("email").value;
    let emailValues = document.createElement("p");
    emailValues.innerHTML = "email :" + email;
    document.body.append(emailValues);

    const dropdownValue = dropdown.options[dropdown.selectedIndex].value;
    let dropdownhtml = document.createElement("p");
    dropdownhtml.innerHTML = "gender :" + dropdownValue;
    document.body.append(dropdownhtml);

    const radioValue = document.querySelector(
      'input[name="radioname"]:checked'
    ).value;
    let radiohtml = document.createElement("p");
    radiohtml.innerHTML = "Role:" + radioValue;
    document.body.append(radiohtml);

    markedCheckbox.forEach((item) => {
      if (item.checked) {
        let formvalues = document.createElement("p");
        formvalues.innerHTML = "permission :" + item.value;
        document.body.append(formvalues);
      }
    });

    let submitButton = document.createElement("INPUT");
    submitButton.setAttribute("type", "submit");
    submitButton.setAttribute("value", "Confirm");
    document.body.append(submitButton);
  }
};

form.addEventListener("submit", (e) => handleSubmit(e));
